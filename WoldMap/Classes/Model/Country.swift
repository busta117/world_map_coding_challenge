//
//  country.swift
//  WoldMap
//
//  Created by Santiago Bustamante on 8/8/16.
//  Copyright © 2016 busta117. All rights reserved.
//

import UIKit

class Country: NSObject {

    var name = ""
    var polygonType:PolygonType = .polygon
    
    var coordinates:[AnyObject] {
        get {
            switch polygonType {
            case .polygon:
                return polygonCoordinates
            case .multiPolygon:
                return multiPolygonCoordinates
            }
        }
        set{
            switch polygonType {
            case .polygon:
                polygonCoordinates = newValue as! [Coordinate]
            case .multiPolygon:
                multiPolygonCoordinates = newValue as! [[Coordinate]]
            }
        }
    }
    
    private var polygonCoordinates:[Coordinate] = []
    private var multiPolygonCoordinates:[[Coordinate]] = []
    
    
    var flag:UIImage?
    
    private var flagBase64:NSData? {
        didSet{
            if let data = flagBase64 {
                flag = UIImage(data: data)
            }
        }
    }
    
    
    init(json:AnyObject){
        super.init()
        
        guard let json = json as? NSDictionary else {
            return
        }
        
        if let type = json["type"] as? String where type != "Feature"{
            return
        }
        
        
        name = (json["properties"]!["name_sort"] as? String) ?? ""
        let typeStr = json["geometry"]!["type"] as! String
        polygonType = PolygonType(rawValue: typeStr)!
        
        let coords = json["geometry"]!["coordinates"] as! [AnyObject]
        
            switch polygonType {
            case .polygon:
                
                if coords.count > 1 {
                    print("alooo")
                }
                
                if let coord = coords[0] as? [AnyObject]{
                    for coord in coord {
                        if let x = coord[0] as? Double, let y = coord[1] as? Double {
                            let coordV = Coordinate(x: x, y: y)
                            coordinates.append(coordV)
                        }
                    }
                }
                
                
            case .multiPolygon:
                
                for coord in coords {
                    
                    if let coord = coord as? [AnyObject]{
                        for coord in coord {
                            if let x = coord[0] as? Double, let y = coord[1] as? Double {
                                coordinates.append(Coordinate(x: x, y: y))
                            }
                        }
                    }
                    
                }
                
            }
        
        
        flagBase64 = NSData(base64EncodedString: (json["properties"]!["flag"] as? String) ?? "", options: NSDataBase64DecodingOptions(rawValue:0))
        
        
    }
    
    
    
}
