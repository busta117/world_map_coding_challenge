//
//  Polygon.swift
//  WoldMap
//
//  Created by Santiago Bustamante on 8/8/16.
//  Copyright © 2016 busta117. All rights reserved.
//

import UIKit



class Coordinate:NSObject {
    var x:Double = 0.0
    var y:Double = 0.0
    
    init(x:Double,y:Double){
        
        let radius = 6378137.0 //radious of the earth
        let maxN = 85.0511287798
        let radians = M_PI/180
        
        self.x = x
        self.y = y
        /*
        self.x = radius * x * radians
        self.y = max(min(maxN, y), -maxN) * radians
        self.y = (radius * log(tan(M_PI/4) + self.y/2)) * -1
    */
        
    }
    
}


enum PolygonType:String{
    case polygon = "Polygon"
    case multiPolygon = "MultiPolygon"
}


class Polygon: NSObject {

    var coordinates:[Coordinate] = []
    
    
}


class MultiPolygon: NSObject {
    
    var coordinates:[[Coordinate]] = []
    
    
}