//
//  mapModel.swift
//  WoldMap
//
//  Created by Santiago Bustamante on 8/8/16.
//  Copyright © 2016 busta117. All rights reserved.
//

import UIKit

class MapModel: NSObject {
    
    
    
    
    class func loadGeoJsonFile(path:String) -> [Country]{
        
        var countries = [Country]()
        
        
        let url = NSBundle.mainBundle().URLForResource(path, withExtension: "geojson")
        let data = NSData(contentsOfURL: url!)
        
        var geoJSON: AnyObject?
        
        do {
            try geoJSON = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
        } catch _ {
            
        }
        

        if let geoJSON = geoJSON as? NSDictionary, features = geoJSON["features"] as? [AnyObject] {
            
            for feature in features {
               countries.append(Country(json: feature))
            }
            
        }
        
        return countries
    }
    
    
    
    
}
