//
//  ViewController.swift
//  WoldMap
//
//  Created by Santiago Bustamante on 8/8/16.
//  Copyright © 2016 busta117. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var countries = [Country]()
    var currentScale:CGFloat = 1.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        contentView.frame = CGRectMake(0, 0, 600, 600)
//        scrollView.contentSize = CGSize(width: 1000, height: 1000)
        
        countries = MapModel.loadGeoJsonFile("countries_small")
        

        drawMap()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func pinchAction(recognizer: UIPinchGestureRecognizer) {
    
        contentView.transform = CGAffineTransformScale(contentView.transform, recognizer.scale, recognizer.scale)
        recognizer.scale = 1
    }
    
    
    
    
    
    func drawMap(){
        for country in countries {
            drawCountry(country)
        }
    }
    
    
    func drawCountry(country:Country){
        
        let shape = CAShapeLayer()
        contentView.layer.addSublayer(shape)
        shape.opacity = 1
        shape.lineWidth = 0.1
        shape.lineJoin = kCALineJoinMiter
        shape.strokeColor = UIColor(white: 0.8,alpha: 1).CGColor
        shape.fillColor = UIColor(white: 0.3, alpha: 1).CGColor
        
        let path = UIBezierPath()
        
        for (index,coord) in country.coordinates.enumerate() {
            if let coord = coord as? Coordinate {
                if index == 0{
                    path.moveToPoint(fixCoords(coord, mapSize: self.view.frame.size))
                    
                }else{
                    path.addLineToPoint(fixCoords(coord, mapSize: self.view.frame.size))
                }
            }
            
        }
        path.closePath()
        shape.path = path.CGPath
    }
    
    
    
    
    func fixCoords(coord:Coordinate, mapSize:CGSize) -> CGPoint{        

        var point = CGPointMake(CGFloat(coord.x), CGFloat(coord.y))
        point.x = mapSize.width + ((point.x*mapSize.width)/(179*89*89))
        point.y = mapSize.height - ((point.y*mapSize.height)/(179*89*89))
        
        point.x = point.x/10 + mapSize.width
        point.y = point.y/10 + mapSize.height*0.7
        
        return point
    }
    

}

